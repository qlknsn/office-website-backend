const client = require("./db.js");
const dbConfig = require("../config/mongo.config.js");
const collection = client.db(dbConfig.database).collection("db_article");
var db = require("mongodb");

class Article {
  constructor(
    title,
    cover,
    summary,
    content,
    externalLink = "",
    sourceFrom = "",
    tags = [],
    userid = ""
  ) {
    this.title = title;
    this.cover = cover;
    this.summary = summary;
    this.content = content;
    this.userid = userid;
    this.tags = tags;
    this.sourceFrom = sourceFrom;
    this.externalLink = externalLink;
  }
  async createArticle() {
    const articleObj = {
      title: this.title,
      cover: this.cover,
      summary: this.summary,
      content: this.content,
      externalLink: this.externalLink,
      sourceFrom: this.sourceFrom,
      userid: this.userid,
      tags: this.tags,
      status: "active", //deleted 代表已经删除， active 代表正常
    };
    const result = await collection.insertOne(articleObj);
    return result;
  }
  static async getArticleList(
    queryObj,
    type = "backend",
    limit = 10,
    current = 1
  ) {
    let skip;
    if (current == 1) {
      skip = 0;
    } else {
      skip = limit * (current - 1);
    }
    
    let projectParams;

    if (type == "website") {
      projectParams = {
        _id: 1,
        title: 1,
        summary: 1,
        userid: 1,
        tags: 1,
        status: 1,
        cover: 1,
        content: 1,
      };
    } else {
      projectParams = {
        _id: 1,
        title: 1,
        summary: 1,
        userid: 1,
        tags: 1,
        cover: 1,
        status: 1,
      };
    }
    return await collection
      .find(queryObj)
      .project(projectParams)
      .limit(limit)
      .skip(skip)
      .sort({ _id: -1 });
  }
  static async getArticleDetail(queryParams) {
    return await collection.findOne(queryParams);
  }
  static async updateArticle(filter, document) {
    const updateDocument = {
      $set: {
        ...document,
      },
    };
    return await collection.updateOne(filter, updateDocument);
  }
}

module.exports = Article;
