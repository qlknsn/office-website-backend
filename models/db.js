const mongoConfig = require("../config/mongo.config.js");

const { MongoClient } = require("mongodb");
const username = encodeURIComponent(mongoConfig.user);
const password = encodeURIComponent(mongoConfig.password);
const clusterUrl = mongoConfig.host;
const port = mongoConfig.port;
const uri = `mongodb://${username}:${password}@${clusterUrl}:${port}/?useUnifiedTopology=true`;
const client = new MongoClient(uri);

async function run() {
  try {
    // Connect the client to the server
    await client.connect();
    // Establish and verify connection
    await client.db("admin").command({ ping: 1 });
    console.log("Connected successfully to server");
  } finally {
    // await client.close();
  }
}

run().catch(console.dir);

module.exports = client;
