class Serialization {
  constructor() {}
  static serializeList(
    data = "",
    code = "1",
    msg = "success",
    limit = 10,
    current = 1,
    total = 0
  ) {
    return {
      code: code,
      msg: msg,
      pagination: { limit: limit, current: current, total: total },
      data: data,
    };
  }
  static serializeObject(data = "", code = "1", msg = "success") {
    return {
      code: code,
      msg: msg,
      data: data,
    };
  }
}

module.exports = Serialization;
