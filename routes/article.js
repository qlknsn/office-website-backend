var express = require("express");
var router = express.Router();
var Article = require("../models/article.js");
var se = require("../serialization/serialization");
const dbConfig = require("../config/mongo.config.js");
const client = require("../models/db.js");
const collection = client.db(dbConfig.database).collection("db_article");
const tools = require("../models/tools");
router.delete("/:articleId", function (req, res, next) {
  let result = Article.updateArticle(
    {
      _id: tools.convertStringID2ObjectID(req.params.articleId),
    },
    { status: "deleted" }
  );
  result.then((r) => {
    res.json(se.serializeObject());
  });
});
router.put("/:articleId", function (req, res, next) {
  let d = req.body;
  let c = Object.assign({}, d);
  delete c._id;

  let result = Article.updateArticle(
    { _id: tools.convertStringID2ObjectID(req.params.articleId) },
    c
  );
  result.then((ers) => {
    res.json(se.serializeObject());
  });
});
router.get("/:articleId", function (req, res, next) {
  let params = req.params;
  let result = Article.getArticleDetail({
    _id: tools.convertStringID2ObjectID(params.articleId),
  });
  result.then((r) => {
    Object.assign(r, {
      createTime: tools.convertID2LocalTime(params.articleId),
    });
    res.json(se.serializeObject(r, undefined, undefined));
  });
});
router.get("/", function (req, res, next) {
  let q = req.query;
  let type = q.type || "backend";
  let result = Article.getArticleList(
    { status: "active" },
    type,
    Number.parseInt(q.limit),
    Number.parseInt(q.current)
  );
  result.then((r) => {
    let list = [];
    async function getArticle() {
      await r.forEach((item) => {
        Object.assign(item, {
          createTime: tools.convertID2LocalTime(item._id),
        });
        list.push(item);
      });
      res.json(
        se.serializeList(
          list,
          undefined,
          undefined,
          Number.parseInt(q.limit),
          Number.parseInt(q.current),
          await collection.find({ status: "active" }).count()
        )
      );
    }
    getArticle();
  });
});
router.post("/", function (req, res, next) {
  let {
    title,
    cover,
    summary,
    content,
    externalLink = "",
    sourceFrom = "",
    tags = [],
  } = req.body;
  let article = new Article(
    title,
    cover,
    summary,
    content,
    externalLink,
    sourceFrom,
    tags
  );
  let r = article.createArticle();
  r.then((ress) => {
    res.json(se.serializeObject((data = ress.ops)));
  });
});
module.exports = router;
