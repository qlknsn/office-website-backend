module.exports = {
  apps: [
    {
      name: "office-website-backend",
      script: "bin/www",
      watch: ".",
      ignore_watch: ["node_modules", ".git"],
      max_memory_restart: "1G",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      // instances: 4,
      // exec_mode: "cluster",
      env: {
        NODE_ENV: "development",
      },
      env_fake: {
        NODE_ENV:"fake",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
  ],

  deploy: {
    fake: {
      user: "frontend",
      host: ["192.168.199.169"],
      ref: "origin/master",
      repo: "http://gitlab.bearhunting.cn/frontend/office-website-backend.git",
      path: "/home/frontend/office-website-backend",
      "pre-deploy-local": "",
      "post-deploy":
        "npm install && pm2 reload ecosystem.config.js --env fake",
      "pre-setup": "",
    },
  },
};
