const client = require("./db.js");
const dbConfig = require("../config/mongo.config.js");
const collection = client.db(dbConfig.database).collection("db_user");
var bcrypt = require("bcrypt");
class User {
  /**
   * 用户
   * @class
   * @classdesc 登录后台的用户
   * @param {string} username - 用户名
   * @param {string} password - 用户密码
   */
  constructor(username, password) {
    this.username = username;
    this.password = password;
  }
  static async validToken(token) {
    return await bcrypt.compare(this.password);
  }
  static async getUserList(queryObj) {
    return await collection.find(queryObj);
  }
  async createUser() {
    const userObj = { username: this.username, password: this.password };
    const existUser = await User.getUserList({ username: this.username });
    let userCount = await existUser.count();
    if (userCount > 0) {
      return Promise.reject("user exist.");
    } else {
      const result = await collection.insertOne(userObj);
      return result;
    }
  }
  static async getUserDetail(queryParams) {
    return await collection.findOne(queryParams);
  }
}

module.exports = User;
