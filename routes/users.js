var express = require("express");
var router = express.Router();
var jwt = require("jsonwebtoken");
var bcrypt = require("bcrypt");
const randStr = "office-website-backend";
var se = require("../serialization/serialization");
var User = require("../models/user");
// 创建用户
router.post("/", async function (req, res, next) {
  let d = req.body;
  let username = d.username;
  let password = d.password;
  let h = await bcrypt.hash(password, 10);
  let u = new User(username, h);
  let r = u.createUser();
  r.then(
    (ress) => {
      res.json(ress);
    },
    (rej) => {
      console.log(rej);
      res.json(se.serializeObject(undefined, 0, rej));
    }
  );
  // let result = await bcrypt.compare(d.password, h);
});
// 登录系统
router.post("/login", async function (req, res, next) {
  let d = req.body;
  let username = d.username;
  let password = d.password;
  const query = { username };
  let user = await User.getUserDetail(query);
  let result = await bcrypt.compare(password, user.password);
  if (result) {
    // 用户名密码正确，生成token
    let token = jwt.sign({ username: username, password: password }, randStr);
    res.json(se.serializeObject((data = { token })));
  } else {
    res
      .status(200)
      .json(se.serializeObject(undefined, 0, "用户名或密码不正确"));
  }
});

module.exports = router;
