var express = require("express");
var router = express.Router();
var Article = require("../models/article.js");
var db = require("mongodb");

/* GET home page. */
router.get("/", function (req, res, next) {
  var article = new Article("test", "123", "123", "123");
  // const r = article.createArticle();
  const s = Article.getArticle();
  s.then((ress) => {
    res.render("index", { title: article.convertID2LocalTime(ress._id) });
  });
});

module.exports = router;
