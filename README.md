## 公司官网后端 API 接口

### 安装系统依赖

- node `12.13.0`及以上
- npm `6.14.9`
- MongoDB 版本为`4.2`及以上
- Nginx 随意
### 正式环境部署

- 安装上一章节所讲的三个依赖
- `npm install`
- 之后运行如下命令安装剩余 npm 包,`npm install -g pm2 mongodb`
- 安装好依赖后在项目根目录下运行`pm2 start ecosystem.config.js`，项目将自动在后台运行。
- 如果代码有更新，pull 最新代码后，运行命令`pm2 restart ecosystem.config.js`即可
- 项目默认开启的是`58118`端口及 API 端口，需要其它端口请用 Nginx 做个代理。

后端代码 Nginx 配置参考代码目录中`backend-nginx.conf`文件。

### 测试环境部署

- 第一次部署服务器

\$ pm2 deploy production setup

- 更新服务器上的代码

\$ pm2 deploy production update

- 回退到 git 上某个版本部署

\$ pm2 deploy production revert 1

- 在服务器上远程执行命令

\$ pm2 deploy production exec "pm2 reload all"

### 初始化数据库

- 手动命令行初始化
- 通过 Navicat 客户端操作

两种方式均可，数据库名字新建为`db_office_website`,需要新建的 collection 有`tb_user`,`tb_article`

### 数据库表字段

具体字段参考源代码目录中的 [models](http://gitlab.bearhunting.cn/frontend/office-website-backend/tree/master/models) 各文件中的定义。

### Mac 运行 MongoDB

To have launchd start mongodb/brew/mongodb-community now and restart at login:
`brew services start mongodb/brew/mongodb-community`

if you don't want/need a background service you can just run:
`mongod --config /usr/local/etc/mongod.conf`
