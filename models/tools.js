var db = require("mongodb");

class Tools {
  constructor() {}
  static convertID2LocalTime(id) {
    var objectId = new db.ObjectID(id);
    return objectId.getTimestamp().toLocaleString();
  }
  static convertStringID2ObjectID(id) {
    return db.ObjectID(id);
  }
}

module.exports = Tools;
