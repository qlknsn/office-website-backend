FROM node:12.13.0
ENV NODE_ENV=producton
WORKDIR /app
COPY ["package.json","package-lock.json*","./"]
RUN npm install  --registry=https://registry.npm.taobao.org
RUN npm install pm2 -g  --registry=https://registry.npm.taobao.org
COPY . .
CMD ["pm2-runtime","start","ecosystem.config.js","--env","production"]