const auth = function (req, res, next) {
  let method = req.method;
  let url = req.originalUrl;
  if (
    (url.includes("users") && method == "POST") ||
    (url.includes("article") && method == "GET")
  ) {
    next();
  } else {
    let token = req.headers["Authorization"] || req.headers["authorization"];
    if (token) {
      // 验证token是否合法

      next();
    } else {
      throw new Error(401);
    }
  }
};

module.exports = auth;
